import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter} from "react-router-dom";
import "./main.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Header from "./layout/Header";
ReactDOM.render(
  <BrowserRouter>
    <Header />
    <header className="bg-white shadow">
      <div className="px-4 py-6 mx-auto max-w-12xl sm:px-6 lg:px-8">
        <h1 className="text-3xl font-bold text-gray-900">
          
        </h1>
      </div>
    </header>
    <main>
      <div className="py-6 mx-auto max-w-12xl sm:px-6 lg:px-8">
        <App />
      </div>
    </main>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
