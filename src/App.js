import './main.css';
import * as React from "react";
import { Routes, Route } from "react-router-dom";

import Home from "./components/Home"
import About from "./components/About"
import Register from "./components/Register"



function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
         <Route path="/Register" element={<Register />} />
      </Routes>
    </div>
  );
}

export default App;
