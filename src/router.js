const routes = [
  {
    name: "Home",
    href: "/",
    current: true,
  },
  { name: "About", 
  href: "/about", 
  current: false },
];

export default routes;
