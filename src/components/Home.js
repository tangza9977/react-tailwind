import * as React from "react";


function Home() {
  return (
    <>
      <div className="grid grid-cols-6">
        <div className="bg-slate-300">01</div>
        <div className="bg-slate-400">02</div>
        <div className="bg-slate-500">03</div>
        <div className="bg-slate-600">04</div>
        <div className="bg-slate-700">05</div>
        <div className="bg-slate-800">06</div>
        <div className="bg-slate-900">07</div>
        <div className="bg-neutral-300">08</div>
        <div className="bg-neutral-400">09</div>
        <div className="bg-neutral-500">10</div>
        <div className="bg-neutral-600">10</div>
        <div className="bg-neutral-700">12</div>
      </div>
      <br></br>

      <div className="columns-6">
      <div className="bg-slate-300">01</div>
        <div className="bg-slate-400">02</div>
        <div className="bg-slate-500">03</div>
        <div className="bg-slate-600">04</div>
        <div className="bg-slate-700">05</div>
        <div className="bg-slate-800">06</div>
        <div className="bg-slate-900">07</div>
        <div className="bg-neutral-300">08</div>
        <div className="bg-neutral-400">09</div>
        <div className="bg-neutral-500">10</div>
        <div className="bg-neutral-600">10</div>
        <div className="bg-neutral-700">12</div>
      </div>
    </>
  );
}

export default Home;
